$(document).ready(function(){
    
    $("#animar").click(function(event){
        $( "#parrafoAnimado" ).animate(
            {
               left: [ "+=50", "swing" ],
               opacity: [ 0.25, "swing" ]
            },
            // Duration
            2000,
            // Callback to invoke when the animation is finished
            function() {
                $( "#parrafoAnimado" ).addClass("rojo");
            }
        );
    });
    
    $("#username").focusout(function(event){
        if($(this).val() == ""){
            $("#usernameContainer").removeClass("has-sucess");
            $("#usernameContainer").addClass("has-error");
        }
        else{
            $("#usernameContainer").removeClass("has-error");
            $("#usernameContainer").addClass("has-success");
        }
    });
    
    $("#ingresar").click(function(event){
        event.preventDefault();
        // Using the core $.ajax() method
        $.ajax({

            // The URL for the request
            url: "resultadoAjax.php",

            // The data to send (will be converted to a query string)
            data: {
                username: $("#username").val(),
                password: $("#password").val()
            },

            // Whether this is a POST or GET request
            type: "POST",

            // The type of data we expect back
            dataType : "text",

            // Code to run if the request succeeds;
            // the response is passed to the function
            success: function( texto ) {
                $("#titulo").text(texto);
                if(texto === "Bienvenido"){
                    $("#miFormulario").fadeOut(1000);
                }
            },

            // Code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function( xhr, status, errorThrown ) {
                alert( "Sorry, there was a problem!" );
                console.log( "Error: " + errorThrown );
                console.log( "Status: " + status );
                console.dir( xhr );
            },
        });
    });
    
    
});