<?php

class Usuario{
    public $nombre;
    public $clave;
    
    function __construct() {
        $this->nombre = "Anonimo";
        $this->clave = "Sin Clave";
        print "Pasé por el constructor de Usuario \n";
    }
    
    public function mostrarNombreClave(){
        return "Nombre: " . $this->nombre . ", Clave: " . $this->clave;
    }
    
    public function saluda(){
        return "Hola " . $this->nombre;
    }
}

